﻿---

<br>

<center style="font-size: 100px; line-height: 1.6">
  <img style="float: left" src="./cobra.jpeg">
  <img style="float: right" src="./e-mma-logo.png" width=170>
    Graphaton
</center>

<br>

---

<h1> 1 - Thème
</h1>

Relevez des défis avec des animations graphiques en 2 manches, notés par un jury en fonction de la qualité visuelle, de la difficulté technique, et de l'originalité !

<h1> 2 - Règles
</h1>

Pour chaque manche, choisissez un défi à relever, et coder (langage/techno libre) une animation graphique en rapport avec le défi.

Vous êtes libre d'interpréter le défi comme vous le souhaitez, tant que vous êtes capable de justifier vos choix.

N'hésitez pas à faire preuve d'originalité. Les easter eggs sont acceptés ;).

<h1> 3 - Déroulement
</h1>

## 3.1 - Manche 1

Choissisez un défi parmi cette liste :

- Paysage
- Électricité
- Nucléaire
- Sinusoïdale
- Rebond
- Horloge
- Dégradé
- Physique et gravité
- Bulles
- Feu d'artifice
- Labyrinthe

Allez rendre votre programme à 13h30.

## 3.2 - Manche 2

Choissisez un défi parmi cette liste :

- Combat
- Lecteur Window Media Player ;)
- Explosion
- Bruit de perlin
- Sons et lumières
- Météo
- Miroir
- Autoroute
- Magie
- Écoulement aquatique
- Architecture d'intérieur

> Pour cette manche 2, vous pouvez ajouter des bonus comme des interactions utilisateurs ou du son.

Allez rendre votre programme à 18h.

<h1> 4 - Merci d'avoir participé !
</h1>

<a href="#top"> ↑ — Revenir en haut </a>
